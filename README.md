# Cardiovascular Disease Detection
# By- Aarush Kumar
#Dated: May 19,2021

I have used KNN and Decision Tree algorithms for Cardiovascular Disease Detection.
According to the Centers for Disease Control and Prevention (CDC), Heart disease is the number one cause of death for men, women, and people of most racial and ethnic groups in the United States.More than one person dies every minute and nearly half a million die each year in the United States from it, costing billions of dollars annually. Therefore, the purpose of this model is to investigate different potential supervised Machine Learning (ML) algorithms for creating diagnostic heart disease models.
In order to conduct this analysis, a model was constructed in Python using the publicly available dataset for heart disease, which has over 300 unique instances with 76 total attributes.From these 76 total attributes, only 14 of them are commonly used for research to this date.
The libraries and coding packages used in this analysis are: Python, NumPy, Matplotlib, Pandas, Scikit-Learn.
Thankyou!
